package com.tenPines.exercise.tusLibros;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TusLibrosApplication {

	public static void main(String[] args) {
		SpringApplication.run(TusLibrosApplication.class, args);
	}

}
